path = require('path');

module.exports = {
  buildModules: ['@nuxt/typescript-build'],
  srcDir: path.resolve(__dirname, 'src'),
};
