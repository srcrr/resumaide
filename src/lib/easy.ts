
var fs = require('fs');
var path = require('path');

let __dirname = path.resolve('');

function readJSON(file_path: string, base_dir = __dirname) : string {
  return JSON.parse(fs.readFileSync(path.resolve(base_dir, file_path), 'utf8'));
}

function readYAML(file_path: string, base_dir = __dirname) : string {
    const YAML = require('yaml');
  return YAML.parse(fs.readFileSync(path.resolve(base_dir, file_path)));
}

export {
    readJSON,
    readYAML
}
