//
// Thanks to this thread: https://github.com/webpack/webpack/issues/4603
// Except that resolveLoader should be resolve.alias
// https://webpack.js.org/configuration/resolve/#resolvealias
//
module.exports = function(source) {
  // console.log(`Source=${source}`);
  // return '';
  return source.replace(/^.*#!.*$/mg, "");
};
