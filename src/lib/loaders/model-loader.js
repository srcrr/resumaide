//
// Thanks to this thread: https://github.com/webpack/webpack/issues/4603
// Except that resolveLoader should be resolve.alias
// https://webpack.js.org/configuration/resolve/#resolvealias
//

import {readYAML, readJSON} from './easy';

module.exports = function(source) {
  // console.log(`Source=${source}`);
  // return '';
  let loader = this;
  function replaceYaml(match, path){
    loader.addDependency(path);
    return readYAML(path);
  }
  function replaceJSON(match, path){
    loader.addDependency(path);
    return readJSON(path);
  }
  source = source.replace('readYaml\((.+)\)', replaceYaml)
                 .replace('readJSON\((.+)\)', replaceJSON);
  console.error(`new source=${source}`);
};
