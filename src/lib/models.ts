import { createModelClass } from '@rawmodel/schema';
const path = require('path');
const filterObject = require('filter-obj');

// Import Resume Schema (https://www.npmjs.com/package/joveo-resume-schema)

const RESUME_JSON = path.resolve('resume-schema',
                                 'schema.json');

let RESUME_SCHEMA = require("resume-schema/schema.json");
// Filter for any element with '$':
RESUME_SCHEMA = filterObject(RESUME_SCHEMA,
                             function (key:string, value:any) {
                                 return (!key.startsWith('$'));
                             }
                         )

// Determine the correct paths.
const SCHEMA_PATH = path.resolve("schemas");
const ROLE_SCHEMA_PATH = path.resolve(SCHEMA_PATH, "role.yaml");
const TEMPLATE_SCHEMA_PATH = path.resolve(SCHEMA_PATH, "template.yaml");

// Construct the schemas
const ROLE_SCHEMA = require('json-loader!yaml-loader!' + "./schemas/role.yaml");
const TEMPLATE_SCHEMA = require('json-loader!yaml-loader!' + "./schemas/template.yaml");

// eslint-disable-next-line
console.debug(`Type of ROLE_SCHEMA=${typeof ROLE_SCHEMA}`);

//
// Models for Resumaide
//

const Resume = createModelClass(RESUME_SCHEMA);
const Role = createModelClass(ROLE_SCHEMA);
const Template = createModelClass(TEMPLATE_SCHEMA);

export {Resume, Role, Template};
