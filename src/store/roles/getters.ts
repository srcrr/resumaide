export default {
    roles: (state:any) => {
        return state.db.find();
    },
    role: (state:any) => (id: number) => {
        return state.db.findOne({_id: id});
    }
};
