
export default {
    templates: (state:any) => {
        return state.db.find();
    },
    template: (state:any) => (id: number) => {
        return state.db.findOne({_id: id});
    }
};
