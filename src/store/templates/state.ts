let DataStore = require('nedb');
let nodePath = require('path');

export default () => ({
    db: new DataStore({
        filename: 'templates.db',
        autoload: true,
    })
})
