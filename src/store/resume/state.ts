let DataStore = require('nedb');
let nodePath = require('path');

export default () => ({
    db: new DataStore({
        filename: 'resume.db',
        autoload: true,
    })
})
